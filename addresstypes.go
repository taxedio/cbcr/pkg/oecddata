package oecddata

type OECD0301 struct {
	Name, Code, Desc string
}

func (c *OECD0301) Init() {
	c.Name = "residentialOrBusiness"
	c.Code = "OECD0301"
	c.Desc = ""
}

type OECD0302 struct {
	Name, Code, Desc string
}

func (c *OECD0302) Init() {
	c.Name = "residential"
	c.Code = "OECD0302"
	c.Desc = ""
}

type OECD0303 struct {
	Name, Code, Desc string
}

func (c *OECD0303) Init() {
	c.Name = "business"
	c.Code = "OECD0303"
	c.Desc = ""
}

type OECD0304 struct {
	Name, Code, Desc string
}

func (c *OECD0304) Init() {
	c.Name = "registeredOffice"
	c.Code = "OECD0304"
	c.Desc = ""
}

type OECD0305 struct {
	Name, Code, Desc string
}

func (c *OECD0305) Init() {
	c.Name = "unspecified"
	c.Code = "OECD0305"
	c.Desc = ""
}
