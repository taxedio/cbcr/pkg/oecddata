package oecddata

type GROUPGAAP struct {
	Name, Code, Desc string
}

func (t *GROUPGAAP) Init() {
	t.Name = "Group GAAP"
	t.Code = "GROUPGAAP"
	t.Desc = "information obtained from entity financial statements prepared in accordance with Group GAAP"
}

type LOCALGAAP struct {
	Name, Code, Desc string
}

func (t *LOCALGAAP) Init() {
	t.Name = "Local GAAP"
	t.Code = "LOCALGAAP"
	t.Desc = "informationt obtained from entity financial statements prepared in accordance with local GAAP"
}

type LOCALLAW struct {
	Name, Code, Desc string
}

func (t *LOCALLAW) Init() {
	t.Name = "Local Law"
	t.Code = "LOCALLAW"
	t.Desc = "information obtained from entity financial statements prepared in accordance with local law"
}

type INTMANACC struct {
	Name, Code, Desc string
}

func (t *INTMANACC) Init() {
	t.Name = "Internal Management Accounts"
	t.Code = "INTMANACC"
	t.Desc = "information obtained from internal management accounts"
}
