package oecddata

type CBC601 struct {
	Name, Code, Desc string
}

func (c *CBC601) Init() {
	c.Name = "Revenues - Unrelated"
	c.Code = "CBC601"
	c.Desc = ""
}

type CBC602 struct {
	Name, Code, Desc string
}

func (c *CBC602) Init() {
	c.Name = "Revenues - Related"
	c.Code = "CBC602"
	c.Desc = ""
}

type CBC603 struct {
	Name, Code, Desc string
}

func (c *CBC603) Init() {
	c.Name = "Revenues - Total"
	c.Code = "CBC603"
	c.Desc = ""
}

type CBC604 struct {
	Name, Code, Desc string
}

func (c *CBC604) Init() {
	c.Name = "Profit or Loss"
	c.Code = "CBC604"
	c.Desc = ""
}

type CBC605 struct {
	Name, Code, Desc string
}

func (c *CBC605) Init() {
	c.Name = "Tax Paid"
	c.Code = "CBC605"
	c.Desc = ""
}

type CBC606 struct {
	Name, Code, Desc string
}

func (c *CBC606) Init() {
	c.Name = "Tax Accrued"
	c.Code = "CBC606"
	c.Desc = ""
}

type CBC607 struct {
	Name, Code, Desc string
}

func (c *CBC607) Init() {
	c.Name = "Capital"
	c.Code = "CBC607"
	c.Desc = ""
}

type CBC608 struct {
	Name, Code, Desc string
}

func (c *CBC608) Init() {
	c.Name = "Earnings"
	c.Code = "CBC608"
	c.Desc = ""
}

type CBC609 struct {
	Name, Code, Desc string
}

func (c *CBC609) Init() {
	c.Name = "Number of Employees"
	c.Code = "CBC609"
	c.Desc = ""
}

type CBC610 struct {
	Name, Code, Desc string
}

func (c *CBC610) Init() {
	c.Name = "Assets"
	c.Code = "CBC610"
	c.Desc = ""
}

type CBC611 struct { // TODO: check if this is correct???
	Name, Code, Desc string
}

func (c *CBC611) Init() {
	c.Name = "Name of MNE Group"
	c.Code = "CBC611"
	c.Desc = ""
}
