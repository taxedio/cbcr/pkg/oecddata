package oecddata

type CBC501 struct {
	Name, Code, Desc string
}

func (c *CBC501) Init() {
	c.Name = "Research and Development"
	c.Code = "CBC501"
	c.Desc = ""
}

type CBC502 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC502) Init() {
	c.Name = "Holding or Managing Intellectual Property"
	c.Code = "CBC502"
	c.Desc = ""
}

type CBC503 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC503) Init() {
	c.Name = "Purchasing or Procurement"
	c.Code = "CBC503"
	c.Desc = ""
}

type CBC504 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC504) Init() {
	c.Name = "Manufacturing or Production"
	c.Code = "CBC504"
	c.Desc = ""
}

type CBC505 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC505) Init() {
	c.Name = "Sales, Marketing or Distribution"
	c.Code = "CBC505"
	c.Desc = ""
}

type CBC506 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC506) Init() {
	c.Name = "Administrative, Management or Support Services"
	c.Code = "CBC506"
	c.Desc = ""
}

type CBC507 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC507) Init() {
	c.Name = "Provision of Services to Unrelated Parties"
	c.Code = "CBC507"
	c.Desc = ""
}

type CBC508 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC508) Init() {
	c.Name = "Internal Group Finance"
	c.Code = "CBC508"
	c.Desc = ""
}

type CBC509 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC509) Init() {
	c.Name = "Regulated Financial Services"
	c.Code = "CBC509"
	c.Desc = ""
}

type CBC510 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC510) Init() {
	c.Name = "Insurance"
	c.Code = "CBC510"
	c.Desc = ""
}

type CBC511 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC511) Init() {
	c.Name = "Holding Shares or Other Equity Instruments"
	c.Code = "CBC511"
	c.Desc = ""
}

type CBC512 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC512) Init() {
	c.Name = "Dormant"
	c.Code = "CBC512"
	c.Desc = ""
}

type CBC513 struct {
	ID               int16
	Name, Code, Desc string
}

func (c *CBC513) Init() {
	c.Name = "Other"
	c.Code = "CBC513"
	c.Desc = ""
}
