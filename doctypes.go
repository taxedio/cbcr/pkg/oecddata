package oecddata

type OECD0 struct {
	Name, Code, Desc string
}

func (o *OECD0) Init() {
	o.Name = "Resent Data"
	o.Code = "OECD0"
	o.Desc = ""
}

type OECD1 struct {
	Name, Code, Desc string
}

func (o *OECD1) Init() {
	o.Name = "New Data"
	o.Code = "OECD1"
	o.Desc = ""
}

type OECD2 struct {
	Name, Code, Desc string
}

func (o *OECD2) Init() {
	o.Name = "Corrected Data"
	o.Code = "OECD2"
	o.Desc = ""
}

type OECD3 struct {
	Name, Code, Desc string
}

func (o *OECD3) Init() {
	o.Name = "Deletion of Data"
	o.Code = "OECD2"
	o.Desc = ""
}

type OECD10 struct {
	Name, Code, Desc string
}

func (o *OECD10) Init() {
	o.Name = "Resent Test Data"
	o.Code = "OECD10"
	o.Desc = ""
}

type OECD11 struct {
	Name, Code, Desc string
}

func (o *OECD11) Init() {
	o.Name = "New Test Data"
	o.Code = "OECD11"
	o.Desc = ""
}

type OECD12 struct {
	Name, Code, Desc string
}

func (o *OECD12) Init() {
	o.Name = "Corrected Test Data"
	o.Code = "OECD12"
	o.Desc = ""
}

type OECD13 struct {
	Name, Code, Desc string
}

func (o *OECD13) Init() {
	o.Name = "Deletion of Test Data"
	o.Code = "OECD13"
	o.Desc = ""
}
