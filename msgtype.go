package oecddata

type CBC401 struct {
	Name, Code, Desc string
}

func (c *CBC401) Init() {
	c.Name = "Message contains new information"
	c.Code = "CBC401"
	c.Desc = ""
}

type CBC402 struct {
	Name, Code, Desc string
}

func (c *CBC402) Init() {
	c.Name = "Message contains corrections/deletions for previously sent information"
	c.Code = "CBC402"
	c.Desc = ""
}
