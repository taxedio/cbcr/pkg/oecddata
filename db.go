package oecddata

type CBCDB CBCRDataPoints

type DataEntry struct {
	ID   string
	Name string
	Code string
	Desc string
}

type CBCRBusinessActs map[string]DataEntry
type CBCRDocummentTypes map[string]DataEntry
type CBCRMessageTypes map[string]DataEntry
type CBCROCEDAddressCodes map[string]DataEntry
type CBCRReportingRoles map[string]DataEntry
type CBCRSummaryCodes map[string]DataEntry
type CBCRXMLCodes map[string]DataEntry
type CBCRTaxedioCodes map[string]DataEntry

type CBCRDataPoints struct {
	BusActs      CBCRBusinessActs
	DocTypes     CBCRDocummentTypes
	MsgType      CBCRMessageTypes
	OECDAddType  CBCROCEDAddressCodes
	RepRoles     CBCRReportingRoles
	SummCodes    CBCRSummaryCodes
	XMLCodes     CBCRXMLCodes
	TaxedioCodes CBCRTaxedioCodes
}

func (c *CBCDB) Init() {
	c.BusActs.Init()
	c.DocTypes.Init()
	c.MsgType.Init()
	c.OECDAddType.Init()
	c.RepRoles.Init()
	c.SummCodes.Init()
	c.XMLCodes.Init()
	c.TaxedioCodes.Init()
}

func (c *CBCRBusinessActs) Init() {
	*c = map[string]DataEntry{
		"CBC501": {
			ID:   "1",
			Name: "Research and Development",
			Code: "CBC501",
			Desc: "TBC",
		},
		"CBC502": {
			ID:   "2",
			Name: "Holding or Managing Intellectual Property",
			Code: "CBC502",
			Desc: "TBC",
		},
		"CBC503": {
			ID:   "3",
			Name: "Purchasing or Procurement",
			Code: "CBC503",
			Desc: "TBC",
		},
		"CBC504": {
			ID:   "4",
			Name: "Manufacturing or Production",
			Code: "CBC504",
			Desc: "TBC",
		},
		"CBC505": {
			ID:   "5",
			Name: "Sales, Marketing or Distribution",
			Code: "CBC505",
			Desc: "TBC",
		},
		"CBC506": {
			ID:   "6",
			Name: "Administrative, Management or Support Services",
			Code: "CBC506",
			Desc: "TBC",
		},
		"CBC507": {
			ID:   "7",
			Name: "Provision of Services to Unrelated Parties",
			Code: "CBC507",
			Desc: "TBC",
		},
		"CBC508": {
			ID:   "8",
			Name: "Internal Group Finance",
			Code: "CBC508",
			Desc: "TBC",
		},
		"CBC509": {
			ID:   "9",
			Name: "Regulated Financial Services",
			Code: "CBC509",
			Desc: "TBC",
		},
		"CBC510": {
			ID:   "10",
			Name: "Insurance",
			Code: "CBC510",
			Desc: "TBC",
		},
		"CBC511": {
			ID:   "11",
			Name: "Holding Shares or Other Equity Instruments",
			Code: "CBC511",
			Desc: "TBC",
		},
		"CBC512": {
			ID:   "12",
			Name: "Dormant",
			Code: "CBC512",
			Desc: "TBC",
		},
		"CBC513": {
			ID:   "13",
			Name: "Other",
			Code: "CBC513",
			Desc: "TBC",
		},
	}
}

func (c *CBCRDocummentTypes) Init() {
	*c = map[string]DataEntry{
		"OECD0": {
			ID:   "1",
			Name: "Resent Data",
			Code: "OECD0",
			Desc: "TBC",
		},
		"OECD1": {
			ID:   "2",
			Name: "New Data",
			Code: "OECD1",
			Desc: "TBC",
		},
		"OECD2": {
			ID:   "3",
			Name: "Corrected Data",
			Code: "OECD2",
			Desc: "TBC",
		},
		"OECD3": {
			ID:   "4",
			Name: "Deletion of Data",
			Code: "OECD3",
			Desc: "TBC",
		},
		"OECD10": {
			ID:   "5",
			Name: "Resent Test Data",
			Code: "OECD10",
			Desc: "TBC",
		},
		"OECD11": {
			ID:   "6",
			Name: "New Test Data",
			Code: "OECD11",
			Desc: "TBC",
		},
		"OECD12": {
			ID:   "7",
			Name: "Corrected Test Data",
			Code: "OECD12",
			Desc: "TBC",
		},
		"OECD13": {
			ID:   "8",
			Name: "Deletion of Test Data",
			Code: "OECD0",
			Desc: "TBC",
		},
	}
}

func (c *CBCRMessageTypes) Init() {
	*c = map[string]DataEntry{
		"CBC401": {
			ID:   "1",
			Name: "Message contains new information",
			Code: "CBC401",
			Desc: "TBC",
		},
		"CBC402": {
			ID:   "2",
			Name: "Message contains corrections/deletions for previously sent information",
			Code: "CBC402",
			Desc: "TBC",
		},
	}
}

func (c *CBCROCEDAddressCodes) Init() {
	*c = map[string]DataEntry{
		"OECD301": {
			ID:   "1",
			Name: "residentialOrBusiness",
			Code: "OECD301",
			Desc: "TBC",
		},
		"OECD302": {
			ID:   "2",
			Name: "residential",
			Code: "OECD302",
			Desc: "TBC",
		},
		"OECD303": {
			ID:   "3",
			Name: "business",
			Code: "OECD303",
			Desc: "TBC",
		},
		"OECD304": {
			ID:   "4",
			Name: "registeredOffice",
			Code: "OECD304",
			Desc: "TBC",
		},
		"OECD305": {
			ID:   "5",
			Name: "unspecified",
			Code: "OECD305",
			Desc: "TBC",
		},
	}
}

func (c *CBCRReportingRoles) Init() {
	*c = map[string]DataEntry{
		"CBC701": {
			ID:   "1",
			Name: "Primary Filing",
			Code: "CBC701",
			Desc: "TBC",
		},
		"CBC702": {
			ID:   "2",
			Name: "Voluntary Filing",
			Code: "CBC702",
			Desc: "TBC",
		},
		"CBC703": {
			ID:   "3",
			Name: "Local Filing",
			Code: "CBC703",
			Desc: "TBC",
		},
		"CBC704": {
			ID:   "4",
			Name: "Local Filing with Incomplete Information",
			Code: "CBC704",
			Desc: "TBC",
		},
	}
}

func (c *CBCRSummaryCodes) Init() {
	*c = map[string]DataEntry{
		"CBC601": {
			ID:   "1",
			Name: "Revenues - Unrelated",
			Code: "CBC601",
			Desc: "TBC",
		},
		"CBC602": {
			ID:   "2",
			Name: "Revenues - Related",
			Code: "CBC602",
			Desc: "TBC",
		},
		"CBC603": {
			ID:   "3",
			Name: "Revenues - Total",
			Code: "CBC603",
			Desc: "TBC",
		},
		"CBC604": {
			ID:   "4",
			Name: "Profit or Loss",
			Code: "CBC604",
			Desc: "TBC",
		},
		"CBC605": {
			ID:   "5",
			Name: "Tax Paid",
			Code: "CBC605",
			Desc: "TBC",
		},
		"CBC606": {
			ID:   "6",
			Name: "Tax Accrued",
			Code: "CBC606",
			Desc: "TBC",
		},
		"CBC607": {
			ID:   "7",
			Name: "Capital",
			Code: "CBC607",
			Desc: "TBC",
		},
		"CBC608": {
			ID:   "8",
			Name: "Earnings",
			Code: "CBC608",
			Desc: "TBC",
		},
		"CBC609": {
			ID:   "9",
			Name: "Number of Employees",
			Code: "CBC609",
			Desc: "TBC",
		},
		"CBC610": {
			ID:   "10",
			Name: "Assets",
			Code: "CBC610",
			Desc: "TBC",
		},
		"CBC611": {
			ID:   "11",
			Name: "Name of MNE Group",
			Code: "CBC611",
			Desc: "TBC",
		},
	}
}

func (c *CBCRXMLCodes) Init() {
	*c = map[string]DataEntry{
		"ENT": {
			ID:   "1",
			Name: "Reporting Entity",
			Code: "ENT",
			Desc: "TBC",
		},
		"REP": {
			ID:   "2",
			Name: "CbCReports",
			Code: "REP",
			Desc: "TBC",
		},
		"ADD": {
			ID:   "3",
			Name: "AdditionalInfo",
			Code: "ADD",
			Desc: "TBC",
		},
	}
}

func (c *CBCRTaxedioCodes) Init() {
	*c = map[string]DataEntry{
		"GROUPGAAP": {
			ID:   "1",
			Name: "Group GAAP",
			Code: "GROUPGAAP",
			Desc: "information obtained from entity financial statements prepared in accordance with Group GAAP",
		},
		"LOCALGAAP": {
			ID:   "2",
			Name: "Local GAAP",
			Code: "LOCALGAAP",
			Desc: "information obtained from entity financial statements prepared in accordance with local GAAP",
		},
		"LOCALLAW": {
			ID:   "3",
			Name: "Local Law",
			Code: "LOCALLAW",
			Desc: "information obtained from entity financial statements prepared in accordance with local law",
		},
		"INTMANACC": {
			ID:   "4",
			Name: "Internal Management Accounts",
			Code: "INTMANACC",
			Desc: "information obtained from internal management accounts",
		},
	}
}
