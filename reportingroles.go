package oecddata

type CBC701 struct {
	Name, Code, Desc string
}

func (c *CBC701) Init() {
	c.Name = "Primary Filing"
	c.Code = "CBC701"
	c.Desc = ""
}

type CBC702 struct {
	Name, Code, Desc string
}

func (c *CBC702) Init() {
	c.Name = "Voluntary Filing"
	c.Code = "CBC702"
	c.Desc = ""
}

type CBC703 struct {
	Name, Code, Desc string
}

func (c *CBC703) Init() {
	c.Name = "Local Filing"
	c.Code = "CBC703"
	c.Desc = ""
}

type CBC704 struct {
	Name, Code, Desc string
}

func (c *CBC704) Init() {
	c.Name = "Local Filing with Incomplete Information"
	c.Code = "CBC704"
	c.Desc = ""
}
