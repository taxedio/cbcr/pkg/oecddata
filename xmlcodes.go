package oecddata

type ENT struct {
	Name, Code, Desc string
}

func (o *ENT) Init() {
	o.Name = "Reporting Entity"
	o.Code = "ENT"
	o.Desc = ""
}

type REP struct {
	Name, Code, Desc string
}

func (o *REP) Init() {
	o.Name = "CbCReports"
	o.Code = "REP"
	o.Desc = ""
}

type ADD struct {
	Name, Code, Desc string
}

func (o *ADD) Init() {
	o.Name = "AdditionalInfo"
	o.Code = "ADD"
	o.Desc = ""
}
