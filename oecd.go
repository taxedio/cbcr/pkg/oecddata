package oecddata

type OECDCode interface {
	code() string
	name() string
	desc() string
}

func Code(o OECDCode) string {
	return o.code()
}

func Name(o OECDCode) string {
	return o.name()
}

func Desc(o OECDCode) string {
	return o.desc()
}
